[![Installer le thème du forum avec Stylus](https://gitlab.com/jesuisfol/theme.forum.rockiemag/raw/master/forums.rockiemag.user.css)

[![Installer le thème du mag avec Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-238b8b.svg)](https://gitlab.com/jesuisfol/theme.forum.rockiemag/raw/master/www.rockiemag.user.css)

# stylish_rockiemag

Thème sombre pour Rockie.

Les couleurs du fond et du texte sont personnalisables, ainsi que la taille du texte :)

# 1. Installer Stylus

**Pour Firefox :**
[https://addons.mozilla.org/fr-FR/firefox/addon/styl-us/](https://addons.mozilla.org/fr-FR/firefox/addon/styl-us/)

**Pour Chrome :**
[https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne/](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne/)

# 2. Installer le thème.
Pour le forum, ça se passe [ici](https://gitlab.com/jesuisfol/theme.forum.rockiemag/raw/master/forums.rockiemag.user.css)
Il devrait y avoir un bouton dans le coin en haut à droite pour ça :)

Il y a aussi un thème pour le mag. Peut-être à retravailler un peu... Il me semble un peu moyen. Ça se passse [par là](https://gitlab.com/jesuisfol/theme.forum.rockiemag/raw/master/www.rockiemag.user.css)
